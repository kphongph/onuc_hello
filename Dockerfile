# Use the official PHP image as the base image
FROM php:latest

# Set the working directory inside the container
WORKDIR /var/www/html

# Copy the local PHP file to the container's working directory
COPY index.php .

# Command to run when the container starts
CMD [ "php", "-S", "0.0.0.0:80" ]
